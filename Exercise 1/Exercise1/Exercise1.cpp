﻿// Exercise1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <algorithm>

int main()
{

	std::string s1 ("hello there");
	//getline(std::cin, s1);
	std::string s2 (s1);
	int count = 0;


	for (int i = 0; i < s2.length(); i++)
	{
		for (int j = 0; j < s1.length(); j++)
		{
			if (s1[j] == s2[i])
			{
				count++;
			}
		}
		if (count > 1)
		{
			s2[i] = ')';
		}
		else
		{
			s2[i] = '(';
		}
		count = 0;
	}

	std::cout << s1<<std::endl;
	std::cout << s2;
	return 0;
}